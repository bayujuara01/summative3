/*
> Bayu Seno Ariefyanto
> Summative 3 Task 7
*/
USE summativedb;
SELECT departement.department_id, departement.department_name, YEAR(employee.hire_date) as join_year, COUNT(employee.employee_id) as total_hired
	FROM employee JOIN departement ON employee.department_id = departement.department_id
    GROUP BY departement.department_name,YEAR(employee.hire_date);