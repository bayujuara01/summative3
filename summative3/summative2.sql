/*
> Bayu Seno Ariefyanto
> Summative 3 Task 2
*/
USE summativedb;
SELECT CONCAT(student.`name`, ' ',student.surname) as fullname, student.birthdate, student.gender, lesson.`name`, lesson.`level`, score.score
	FROM student JOIN score ON student.id = score.student_id
		JOIN lesson ON lesson.id = score.lesson_id;