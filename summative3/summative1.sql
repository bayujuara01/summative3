/*
> Bayu Seno Ariefyanto
> Summative 3 Task 1
*/
CREATE DATABASE summativedb;
USE summativedb;

CREATE TABLE student (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,
    surname VARCHAR(60),
    birthdate DATE not null,
    gender ENUM('male', 'female')
);

CREATE TABLE lesson (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(120) NOT NULL,
    `level` INT NOT NULL
);

CREATE TABLE score (
	id INT PRIMARY KEY AUTO_INCREMENT,
    student_id INT NOT NULL,
    lesson_id INT NOT NULL,
    score INT NOT NULL,
    CONSTRAINT FK_Student_Score FOREIGN KEY (student_id) REFERENCES student (id),
    CONSTRAINT FK_Lesson_Score FOREIGN KEY (lesson_id) REFERENCES lesson (id)
);

INSERT INTO student (`name`, surname, birthdate, gender) VALUES 
('Bayu Seno', 'Ariefyanto', '1999-06-09', 'male'),
('Reza', 'Pahlevi', '1998-06-06', 'male'),
('Kheisya', 'Tahlita', '2001-10-10', 'female'),
('Anisa', 'M. Fitriyanti', '2000-01-01', 'female'),
('Anatasya', 'Eizelia', '2000-04-21', 'female');

INSERT INTO lesson (`name`, `level`) VALUES 
('Object Oriented Programming', 1),
('Mobile Programming', 2),
('Web Development', 1);

INSERT INTO score (student_id, lesson_id, score) VALUES 
(1, 1, 100), (1, 2, 100), (1, 3, 100),
(2, 1, 87), (2, 2, 83), (2, 3, 85), 
(3, 1, 97), (3, 2, 93), (3, 3, 95), 
(4, 1, 77), (4, 2, 73), (4, 3, 75),
(5, 1, 67), (5, 2, 63), (5, 3, 65);