/*
> Bayu Seno Ariefyanto
> Summative 3 Task 3
*/
USE summativedb;
CREATE TABLE employee (
	employee_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(60) NOT NULL,
    last_name VARCHAR(60),
    email VARCHAR(60) NOT NULL, 
    phone_number VARCHAR(20) NOT NULL,
    hire_date DATE NOT NULL,
    job_id VARCHAR(10) NOT NULL,
    salary DECIMAL(10,2) NOT NULL DEFAULT 0.00,
    commission_pct DECIMAL (10,2) DEFAULT 0.00,
    manager_id INT NOT NULL,
    department_id INT NOT NULL
);

/* 
> Dengan melakukan set global local_infile=1, maka kita dapat melakukan load (import) data.
> Melakukan import data dengan sintaks LOAD DATA LOCAL INFILE dan disertai path file yang akan diimport,
  tiap PC akan menghasilkan path yang berbeda.
> data import file tadi dimasukkan pada tabel employee dan sesuaikan dengan format pada file.
*/
SET GLOBAL local_infile=1;
LOAD DATA LOCAL INFILE 'C:/Users/NEXSOFT/Documents/Bootcamp/Summative/summative3/summative2.txt'  
	INTO TABLE employee
    FIELDS TERMINATED BY '|'
    LINES STARTING BY '|' TERMINATED BY '\n'
    IGNORE 2 LINES;