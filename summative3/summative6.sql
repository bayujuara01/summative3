/*
> Bayu Seno Ariefyanto
> Summative 3 Task 6
*/
USE summativedb;
CREATE TABLE departement (
	department_id INT PRIMARY KEY AUTO_INCREMENT,
    department_name VARCHAR(40) NOT NULL,
    manager_id INT NOT NULL,
    location_id INT NOT NULL
);

/* 
> Dengan melakukan set global local_infile=1, maka kita dapat melakukan load (import) data.
> Melakukan import data dengan sintaks LOAD DATA LOCAL INFILE dan disertai path file yang akan diimport,
  tiap PC akan menghasilkan path yang berbeda.
> data import file tadi dimasukkan pada tabel employee dan sesuaikan dengan format pada file.
*/
SET GLOBAL local_infile=1;
LOAD DATA LOCAL INFILE 'C:/Users/NEXSOFT/Documents/Bootcamp/Summative/summative3/summative2-1.txt'  
	INTO TABLE departement
    FIELDS TERMINATED BY '|'
    LINES STARTING BY '|' TERMINATED BY '\n'
    IGNORE 2 LINES;