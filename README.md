# summative3

## Nexsoft Fun Coding Bootcamp Batch 7 Day 15
### Summative 3 - SQL

## File Directory
```
summative3
│   README.md
│   summative2.txt
│   summative2-1.txt
└───summative3
    │   summative1.sql
    │   summative2.sql
    │   summative3.sql
    │   summative4.sql
    │   summative5.sql
    │   summative6.sql
    │   summative7.sql
```

## Author
### Bayu Seno Ariefyanto
